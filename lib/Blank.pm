package Blank;

use strict;
use warnings;

our $version = '0.01';

sub hello_w{
	print "hello world\n";
	return "hello";
}

1;
__END__

=head1 NAME

Blank - Perl blank template cpan style module distribution

=head1 SYNOPSIS

	use Blank;

=head1 DESCRIPTION

	A blank template module for those wishing to create a perl module 

=head2 EXPORT

None by default

=head1 SEE ALSO

=head1 AUTHOR

Aaron Spencer, E<lt>spaaron@umich.eduE<gt>

=head1 COPYRIGHT AND LICENSE

Copyright (C) by Aaron Spencer

This library is free software; you can redistribute it and/or modify 
it under the same terms as Perl itself, either Perl version 5.20.0, or,
at your option, any later version of Perl 5 you may have available.

=cut
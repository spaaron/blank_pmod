# blank_pmod

### Generate a starter perl module 
install modulemaker. [More info here](https://metacpan.org/pod/ExtUtils::ModuleMaker)   

~~~cmd
$ sudo apt-get install modulemaker
$ modulemaker
~~~


If you specify
~~~txt 
Module::Build 
~~~ 
during the prompts of the modulemaker,
it will generate something similar to the contents of this repo.

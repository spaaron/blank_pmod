# Before 'make install' is performed this script should be runnable with
# 'make test'. After 'make install' it should work as 'perl Blank.t'

use strict;
use warnings;

##########################
use Test::More tests => 1;
#simple compilation check
BEGIN { use_ok('Blank') };
##########################
